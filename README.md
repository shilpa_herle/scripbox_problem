# Simple NoSQL database

This script implements a simple NoSQL database with JSON file
backing. The database contents are naively read into memory,
manipulated and written back at the end of the script.

Usage:

    usage: simple_nosql_db.py [-h] [--filename FILENAME] [--key KEY] [--value VALUE] [--column COLUMN]
                              cmd
    
    positional arguments:
      cmd                  Command to execute - init, add, delete, modify, find
    
    optional arguments:
      -h, --help           show this help message and exit
      --filename FILENAME  DB file name, db.json by default
      --key KEY            Key to use for add, delete, modify, find
      --value VALUE        Value to add or modify
      --column COLUMN      Column to modify

## Initialize empty database

    > ./simple_nosql_db.py init [--filename file.json]

## Add a key, value pair

    > ./simple_nosql_db.py add [--filename file.json] --key a --value \
        '{ "column1": value1, "column2", value2 }'

## Modify a value or column

The modify command has 2 variants.

* Modify the whole value for a key

    > ./simple_nosql_db.py modify [--filename file.json] --key a \
          --value '{ "column1": value3, "column2", value4 }'

* Modify a column within the value for a key (if value is an object)

    > ./simple_nosql_db.py modify [--filename file.json] --key a \
          --column column1 --value 1234

## Delete a key

    > ./simple_nosql_db.py delete [--filename file.json] --key a

## Print value for a key

    > ./simple_nosql_db.py find [--filename file.json] --key a
