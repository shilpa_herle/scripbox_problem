#!/usr/bin/python3.7

# Simple NoSQL DB with JSON file backing
# Loads file into memory and writes back at exit
# Supports init, add, modify, delete, find

import json
import argparse

def init_db(dbh):
    """Truncate the DB file"""
    dbh.truncate(0)

def load_db(dbh):
    """Load DB file and return object"""
    try:
        db = json.load(dbh)
    except Exception as e:
        print("ERROR: load DB: {}".format(e))
        db = None
    return db

def _decode_record(record):
    """Record can be JSON or string"""
    try:
        record = json.loads(args.value)
    except:
        print("Record is not JSON, saving as string")
        record = args.value
    return record

def add_record(db, key, record):
    """Add a key, value pair into DB"""
    if key in db:
        print("ERROR: Key {} exists, use modify command to overwrite".format(key))
    else:
        db[key] = record

def modify_record(db, key, column, record):
    """
    Modify value of given key
    If column is not None, modify value of column under key
    """
    if key in db:
        if column is None:
            db[key] = record
        elif type(db[key]) is dict and column in db[key]:
            db[key][column] = record
        else:
            print("ERROR: Column {} not found in key {}".format(column, key))
    else:
        print("ERROR: Key {} does not exist".format(key))

def delete_record(db, key):
    """Delete value of given key"""
    if key in db:
        del db[key]
    else:
        print("WARN: Non-existent key {}".format(key))

def find_record(db, key):
    """Find key in DB"""
    if key in db:
        return db[key]
    else:
        return None

def save_db(dbh, db):
    """Overwrite input file with updated DB object"""
    dbh.truncate(0)
    dbh.seek(0)
    json.dump(db, dbh, indent=2, sort_keys=True)

# Main
# Options
parser = argparse.ArgumentParser()
parser.add_argument("cmd", help="Command to execute - init, add, delete, modify, find")
parser.add_argument("--filename", help="DB file name, db.json by default")
parser.add_argument("--key", help="Key to use for add, delete, modify, find")
parser.add_argument("--value", help="Value to add or modify, can be JSON or plain string")
parser.add_argument("--column", help="Column to modify")
args = parser.parse_args()

if args.cmd == "init":
    mode = 'w+'
else:
    mode = 'r+'

if args.filename:
    filename = args.filename
else:
    filename = 'db.json'

db = None
modified = False
try:
    with open(filename, mode) as dbh:
        if args.cmd == "init":
            init_db(dbh)
            db = {}
            modified = True
        else:
            db = load_db(dbh)
except Exception as e:
    print("ERROR: Cannot open file {}".format(filename))
    db = None

if db is not None:
    if args.cmd == "add":
        if args.key is None or args.value is None:
            print("ERROR: --key and --value required for add")
        else:
            record = _decode_record(args.value)
            add_record(db, args.key, record)
            modified = True
    elif args.cmd == "delete":
        if args.key is None:
            print("ERROR: --key required for delete")
        else:
            delete_record(db, args.key)
            modified = True
    elif args.cmd == "modify":
        if args.key is None or args.value is None:
            print("ERROR: --key and --value required for modify")
        else:
            record = _decode_record(args.value)
            modify_record(db, args.key, args.column, record)
            modified = True
    elif args.cmd == "find":
        if args.key is None:
            print("ERROR: --key required for find")
        else:
            print("Key {} = {}".format(args.key, find_record(db, args.key)))

if modified:
    print("Saving modifications to {}".format(filename))
    try:
        with open(filename, 'w+') as dbh:
            save_db(dbh, db)
    except Exception as e:
        print("ERROR: Cannot save to file {}".format(filename))
